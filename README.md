> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Advance Web Applications 

## Shelby Shuford 

### Assignment 3 # Requirements:

*Sub-Heading:*

1. ERD 
2. 10 Records per talbe 
3. Read me file with access 
4. a3.mwb
5. a3.png 
5.BB links  

#### README.md file should include the following items:

* a3.png  
* sa3.mwb

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>  

#### Assignment Screenshots:

path "" title !

![erd](/erd.png)

#### Tutorial Links:

*A3 ERD*
[A3 SQL](/a3.sql/ "SQL A3")

*A3 ERD *
[A3 MWB](/lisa3.mwb/ "A3 SQL")